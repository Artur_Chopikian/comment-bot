import re
import time
import sys

from selenium import webdriver

from selenium.common.exceptions import TimeoutException

from selenium.webdriver.common.keys import Keys

from selenium.webdriver.common.by import By

from selenium.webdriver.support.ui import WebDriverWait

from selenium.webdriver.support import expected_conditions as ec

from selenium.webdriver.remote.webelement import WebElement

from selenium.webdriver.common.action_chains import ActionChains

from time import sleep

from config import PASSWORD


def find_by_xpath(browser: webdriver, xpath: str, wait_time: int = 5) -> WebElement:
    elem = WebDriverWait(browser, wait_time).until(
        ec.presence_of_element_located(
            (By.XPATH, xpath)))
    return elem


def find_by_class_name(browser: webdriver, class_name: str, wait_time: int = 5) -> WebElement:
    elem = WebDriverWait(browser, wait_time).until(
        ec.presence_of_element_located(
            (By.CLASS_NAME, class_name)))
    return elem


class CommentBot(object):
    """ Бот для ставлення коментарів під постами в інстаграмі """

    def __init__(self):
        self.options = webdriver.ChromeOptions()
        self.options.add_argument('headless')
        # self.browser = webdriver.Chrome('../driver/chromedriver',
        #                                 options=self.options)
        self.browser = webdriver.Chrome('../driver/chromedriver')
        self.path_to_file = r'../comment_bot_files'

    def login_account(self, login: str, password: str):
        """ Вхід на акаунт """
        self.browser.get('https://www.instagram.com/accounts/login/?source=auth_switcher')

        try:
            self.browser.implicitly_wait(10)
            input_login = WebDriverWait(self.browser, 5).until(
                ec.presence_of_element_located(
                    (By.XPATH, '//*[@id="loginForm"]/div/div[1]/div/label/input')))
            input_password = WebDriverWait(self.browser, 5).until(
                ec.presence_of_element_located(
                    (By.XPATH, '//*[@id="loginForm"]/div/div[2]/div/label/input')))
            input_login.send_keys(login, Keys.ENTER)
            input_password.send_keys(password, Keys.ENTER)
            try:
                self.browser.implicitly_wait(5)
                error_login = WebDriverWait(self.browser, 5).until(
                    ec.presence_of_element_located((By.XPATH, '//div[@class="eiCW-"]//p')))
                return str(error_login.text)

            except TimeoutException:
                self.browser.get('https://www.instagram.com/' + login)
                try:
                    self.browser.implicitly_wait(5)
                    number_following = WebDriverWait(self.browser, 5).until(
                        ec.presence_of_element_located(
                            (By.XPATH, '//section/main/div/header/section/ul/li[3]/a/span')))
                    return int(number_following.text)  # return numbers followings

                except TimeoutException:
                    return 'Не вдалося увійти.'

        except TimeoutException:
            return 'Елементів не вдалося найти, перевірте піднючення до інтернету.'

    def create_file_with_following(self, number_following: int, login: str):
        """ Створення файлу з іменами фоловерів """
        list_filter = ['shop']
        sleep(5)
        self.browser.get('https://www.instagram.com/' + login)

        button_following = find_by_xpath(self.browser, '//section/main/div/header/section/ul/li[3]/a')
        button_following.click()

        scroll_element = find_by_xpath(self.browser, '//div[@class="isgrP"]')

        for i in range(5):
            self.browser.execute_script('arguments[0].scrollTop = arguments[0].scrollHeight/%s' % (5 - i),
                                        scroll_element)
            sleep(.8)

        with open(self.path_to_file + login + '.txt', 'w') as file:
            print('Open file')

            for list_element in range(1, number_following + 1):
                try:
                    self.browser.execute_script('arguments[0].scrollTop = arguments[0].scrollHeight', scroll_element)
                    sleep(.1)

                    person_name = self.browser.find_element_by_xpath(
                        f'//div[@class="PZuss"]/li[{list_element}]/div/div[1]/div[2]/div[1]/span/a')

                    print(person_name.text, list_element)

                    # return value for progress bar
                    yield list_element
                    # filter person_name
                    for name in list_filter:
                        if name not in person_name.text:
                            file.write(f'@{person_name.text}\n')

                except TimeoutException:
                    print('Element not found')
                    break

    def distribution_comments(self, login: str, url_post: str, names_in_comment: int, comments: int):
        """   Розподіл коментарів  """
        print('start')

        number_comment = 0

        self.browser.implicitly_wait(5)
        self.browser.get(url_post)

        list_name = self.read_file(login)

        for index in range(0, comments * names_in_comment, names_in_comment):
            comment_text = ' '.join(list_name[index:index + names_in_comment])
            # - - - - - - - - - - - -
            if len(comment_text.split()) == names_in_comment:
                function = self.add_comment(comment_text)
                if function:
                    number_comment += 1
                    yield number_comment
                elif type(function) == str:
                    yield function
                    break
            else:
                break
            # - - - - - - - - - - - -
        del list_name[0:number_comment * names_in_comment]
        self.write_file(list_name, login)
        self.browser.close()

    def add_comment_text(self, url_post, number_comments, comment_text):

        self.browser.implicitly_wait(5)
        self.browser.get(url_post)

        for _ in range(number_comments):
            self.add_comment(comment_text)
            print(f'add {_} comment')

    def add_comment(self, comment_text):
        """ Додавання коментаря  """
        try:
            sleep(40)
            self.browser.implicitly_wait(10)
            textarea = WebDriverWait(self.browser, 5).until(
                ec.presence_of_element_located(
                    (
                        By.XPATH,
                        '//textarea[@class="Ypffh"]')))
            textarea.click()
            # //section/main/div/div/article/div[3]/section[4]/div/form/textarea
            textarea_click = WebDriverWait(self.browser, 5).until(
                ec.presence_of_element_located(
                    (By.XPATH,
                     '//textarea[@class="Ypffh focus-visible"]')))
            textarea_click.send_keys(comment_text)
            # textarea_click.send_keys('хочу выиграть!')
            textarea_click.submit()
            try:
                error = WebDriverWait(self.browser, 5).until(
                    ec.presence_of_element_located(
                        (By.CSS_SELECTOR, 'body > div.Z2m7o > div > div > div > p')))
                if error:
                    return error.text
                print(error)
            except TimeoutException:
                try:
                    action_blocked = WebDriverWait(self.browser, 5).until(
                        ec.presence_of_element_located(
                            (By.XPATH, '/html/body/div[4]/div/div')))
                    if action_blocked:
                        return action_blocked.text

                except TimeoutException:
                    return True

        except TimeoutException:
            return 'Поля для воду коментарів не найдено, перевірте підключення до інтернету.'

    def read_file(self, user_login):
        with open(self.path_to_file + user_login + '.txt', 'r') as file_list_name:
            list_name = file_list_name.readlines()
        list_name = [line.replace('\n', '') for line in list_name]
        return list_name

    def write_file(self, list_name, user_login):
        with open(self.path_to_file + user_login + '.txt', 'w') as file:
            for line in list_name:
                file.write(line + '\n')

    def skraping_giveaways(self):
        searches_field = find_by_xpath(self.browser, '//*[@id="react-root"]/section/nav/div[2]/div/div/div[2]/input')
        searches_field.send_keys('#giveaway')
        time.sleep(2)

        # clicl in first item in list
        find_by_xpath(self.browser,
                      '//*[@id="react-root"]/section/nav/div[2]/div/div/div[2]/div[3]/div/div[2]/div/div[1]/a').click()

        # elem = find_by_xpath(self.browser,
        #                      '//*[@id="react-root"]/section/main/article/div[1]/div/div/div[1]/div[1]/a/div/div[2]')

        # elem = find_by_xpath(self.browser, '//div[@class="EZdmt"]/div/div/div[1]/div/a/div/div[2]')

        # //*[@id="react-root"]/section/main/article/div[1]/div/div/div[1]/div[1]/a/div/div[2]
        # //*[@id="react-root"]/section/main/article/div[1]/div/div/div[1]/div[2]/a/div/div[2]
        # //*[@id="react-root"]/section/main/article/div[1]/div/div/div[1]/div[3]/a/div/div[2]

        # //*[@id="react-root"]/section/main/article/div[1]/div/div/div[2]/div[1]/a/div/div[2]
        # //*[@id="react-root"]/section/main/article/div[1]/div/div/div[2]/div[2]/a/div/div[2]
        # //*[@id="react-root"]/section/main/article/div[1]/div/div/div[2]/div[3]/a/div/div[2]

        for line in range(1, 20):
            self.browser.execute_script(f"window.scrollTo(0, 232.33)")
            print(f'- - {line} - -')
            time.sleep(.3)
            for item in range(1, 4):
                # elem = find_by_xpath(self.browser, f'//div[@class="EZdmt"]/div/div/div[{line}]/div[{item}]/a/div/div[2]')
                elem = find_by_xpath(self.browser, f'//article/div[2]/div/div[{line}]/div[{item}]/a/div/div[2]')

                hover = ActionChains(self.browser).move_to_element(elem)
                hover.perform()

                numbers_of_coments = find_by_class_name(self.browser, 'qn-0x')

                source_code = numbers_of_coments.get_attribute('innerHTML')

                # print(source_code)
                print(re.findall(r'\b(\d+)\b', source_code.replace(' ', '')))


if __name__ == '__main__':
    pass
