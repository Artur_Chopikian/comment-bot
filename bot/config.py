from environs import Env

env = Env()
env.read_env()

FAKE_ACCOUNTS = env.list('FAKE_ACCOUNTS')
PASSWORD = env.str('PASSWORD')
