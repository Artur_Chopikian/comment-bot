import threading

import tkinter as tk

from tkinter import ttk

from tkinter import messagebox

from bot.comment_bot import CommentBot

key = ['login', 'password', 'url_post', 'names_in_comment', 'comments']


def parallel_execution(func):
    def execute(*args, **kwargs):
        threading.Thread(target=func, args=args, kwargs=kwargs).start()

    return execute


class GUI(object):
    data = []
    list_element_frame = []
    list_checkbutton = []
    list_all_len_file = []

    list_name = []
    list_url_post = []
    list_names_in_comment = []
    list_number_comments = []
    list_lable_len_file = []

    def __init__(self, master):

        self.read_account_file()
        self.get_len_list()

        self.master = master
        self.master.title('Comment bot')
        self.master.geometry('720x500')
        self.master.resizable(0, 0)

        self.list_account_frame = ttk.Frame(self.master)

        self.canvas = tk.Canvas(self.list_account_frame, width=700, height=500)
        self.scrollbar = ttk.Scrollbar(self.list_account_frame, orient="vertical", command=self.canvas.yview)
        self.scrollbar_frame = ttk.Frame(self.canvas, width=700)

        self.scrollbar_frame.bind("<Configure>", lambda e: self.canvas.configure(scrollregion=self.canvas.bbox("all")))

        self.canvas.create_window((0, 0), window=self.scrollbar_frame, anchor="nw")
        self.canvas.configure(yscrollcommand=self.scrollbar.set)

        self.update_list_account()

    def __del__(self):
        return self.update_account_file()

    def update_list_account(self):
        """ create list accounts in main window with scroll bar """
        self.update_account_file()
        self.get_len_list()

        self.list_checkbutton = [tk.BooleanVar() for i in range(len(self.data))]

        button_list_start, button_list_edit, button_list_delete = [], [], []

        for i in range(len(self.data)):
            self.list_element_frame.append(tk.Frame(self.scrollbar_frame))

            self.list_element_frame[i].pack(side=tk.TOP, padx=1, pady=1)
            # ------------------------------------------ info about users ----------------------------------------------
            self.list_name.append(
                tk.Label(self.list_element_frame[i], text=self.data[i]['login'], width=10, height=2))
            self.list_name[i].grid(row=0, column=0, padx=1, pady=1)

            self.list_url_post.append(
                tk.Label(self.list_element_frame[i], text=self.data[i]['url_post'], width=35, height=2))
            self.list_url_post[i].grid(row=0, column=1, padx=1, pady=1)

            self.list_names_in_comment.append(
                tk.Label(self.list_element_frame[i], text=self.data[i]['names_in_comment'], width=3, height=2))
            self.list_names_in_comment[i].grid(row=0, column=2, padx=1, pady=1)

            self.list_number_comments.append(
                tk.Label(self.list_element_frame[i], text=self.data[i]['comments'], width=3, height=2))
            self.list_number_comments[i].grid(row=0, column=3, padx=1, pady=1)

            ttk.Checkbutton(self.list_element_frame[i], variable=self.list_checkbutton[i]) \
                .grid(row=0, column=4, padx=1, pady=1)

            self.list_lable_len_file.append(
                tk.Label(self.list_element_frame[i], text=f'{self.list_all_len_file[i]}', width=3, height=2))
            self.list_lable_len_file[i].grid(row=0, column=5, padx=1, pady=1)

            button_list_start.append(tk.Button(self.list_element_frame[i], text='Start', width=5, height=2,
                                               command=lambda account=i: self.click_start(account)))
            button_list_start[i].grid(row=0, column=6, padx=1, pady=1)

            button_list_edit.append(tk.Button(self.list_element_frame[i], text='Edit', width=5, height=2,
                                              command=lambda account=i: self.click_edit(account)))
            button_list_edit[i].grid(row=0, column=7, padx=1, pady=1)

            button_list_delete.append(tk.Button(self.list_element_frame[i], text='Delete', width=5, height=2,
                                                command=lambda account=i: self.click_delete(account)))
            button_list_delete[i].grid(row=0, column=8, padx=1, pady=1)

            # ------------------------------------------ info about users ----------------------------------------------
            self.list_account_frame.pack()
            self.canvas.pack(side="left", fill="both", expand=True)
            self.scrollbar.pack(side="right", fill="y")

    @parallel_execution
    def click_start(self, account):
        """ start comment bot  """
        obj = CommentBot()
        number_following = obj.login_account(login=self.data[account]['login'],
                                             password=self.data[account]['password'])
        if type(number_following) == int:
            if self.list_checkbutton[account].get():
                self.open_progress_bar('Create file: ', obj, account, number_following)
            self.open_progress_bar('Add comment: ', obj, account)
        else:
            messagebox.showerror(message=number_following[1])

    def open_progress_bar(self, text, obj, account, number_following=None):
        progress_frame = tk.Frame(self.list_element_frame[account])
        progress_frame.grid(row=1, columnspan=9)

        text_for_progress_bar = tk.Label(progress_frame, text=text)
        text_for_progress_bar.grid(row=0, column=0)

        progress_bar = ttk.Progressbar(progress_frame,
                                       orient=tk.HORIZONTAL,
                                       length=400,
                                       mode='determinate')
        progress_bar.grid(row=0, column=1)

        if number_following is not None:
            progress_bar['maximum'] = number_following
            for i in obj.create_file_with_following(number_following, self.data[account]['login']):
                progress_bar['value'] = i
        else:
            number_comment = int(self.data[account]['comments'])
            progress_bar['maximum'] = number_comment

            for i in obj.distribution_comments(login=self.data[account]['login'],
                                               url_post=self.data[account]['url_post'],
                                               names_in_comment=int(self.data[account]['names_in_comment']),
                                               comments=int(self.data[account]['comments'])):
                print(i)
                if type(i) == int:
                    progress_bar['value'] = i
                else:
                    messagebox.showerror(message=i)

        self.list_lable_len_file[account]['text'] = self.get_len_list(one_name=self.data[account]['login'])
        progress_frame.grid_forget()

    def click_delete(self, account):
        del self.data[account]
        del self.list_name[account]
        del self.list_url_post[account]
        del self.list_names_in_comment[account]
        del self.list_number_comments[account]
        del self.list_lable_len_file[account]

        self.list_element_frame[account].destroy()
        self.update_list_account()

    def click_edit(self, account):
        """ create edit window """
        global key
        edit_window = tk.Toplevel(self.master)

        list_entry, text_entry = [], [tk.StringVar(), tk.StringVar(), tk.StringVar(), tk.StringVar(), tk.StringVar()]

        for index in range(len(key)):
            tk.Label(edit_window, text=key[index].title(), height=2).grid(row=index, column=0, sticky='nw')
            list_entry.append(tk.Entry(edit_window, textvariable=text_entry[index]))
            list_entry[index].grid(row=index, column=1)
            list_entry[index].insert(0, self.data[account][key[index]])

        button_cancel_edit = tk.Button(edit_window, text='Cancel', height=2,
                                       command=lambda: edit_window.destroy())
        button_cancel_edit.grid(row=5, column=0, sticky='W' + 'E')

        button_confirm_edit = tk.Button(edit_window, text='Confirm', height=2,
                                        command=lambda: self.click_confirm_edit(text_entry, account, edit_window))
        button_confirm_edit.grid(row=5, column=1, sticky='W' + 'E')

    def click_confirm_edit(self, text_entry, account, edit_window):
        """ write edit data in file  """
        global key
        for index in range(len(text_entry)):
            self.data[account][key[index]] = text_entry[index].get()
        self.list_name[account]['text'] = self.data[account]['login']
        self.list_url_post[account]['text'] = self.data[account]['url_post']
        self.list_names_in_comment[account]['text'] = self.data[account]['names_in_comment']
        self.list_number_comments[account]['text'] = self.data[account]['comments']
        self.list_lable_len_file[account]['text'] = self.get_len_list(one_name=self.data[account]['login'])
        self.update_account_file()
        edit_window.destroy()

    def read_account_file(self):
        """ read list accounts with file """
        global key
        with open('comment_bot_files/account_file.txt', 'r') as file:
            data = file.readlines()
        data = [i.split() for i in data]
        for i in range(len(data)):
            self.data.append(dict(zip(key, data[i])))

    def update_account_file(self):
        """ write list accounts in file """
        global key
        lines = [[self.data[i][j] for j in key] for i in range(len(self.data))]
        with open('comment_bot_files/account_file.txt', 'w') as file:
            for line in lines:
                file.write(' '.join(line) + '\n')

    def get_len_list(self, one_name=None):
        if one_name is not None:
            try:
                with open(r'/Users/ArturChopikian/PycharmProjects/CommentBot/comment_bot_files/' +
                          one_name + '.txt', 'r') as file_list_name:
                    list_name = file_list_name.readlines()
                return str(len(list_name))
            except FileNotFoundError:
                return '-'
        else:
            self.list_all_len_file = []
            for i in range(len(self.data)):
                try:
                    with open(r'/Users/ArturChopikian/PycharmProjects/CommentBot/comment_bot_files/' +
                              self.data[i]['login'] + '.txt', 'r') as file_list_name:
                        list_name = file_list_name.readlines()
                    self.list_all_len_file.append(len(list_name))
                except FileNotFoundError:
                    self.list_all_len_file.append('-')


def main():
    root = tk.Tk()
    GUI(root)
    root.mainloop()


if __name__ == '__main__':
    main()
